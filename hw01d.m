A = bucky();
[n, ~] = size(A);
A1 = A + 5*speye(n); 
A2 = A + 9*speye(n);
A3 = rand(n,n) + 66*eye(n);
 
x0 = ones(n, 1);
tol = 0.0000000001;
maxit = 100;
 
b = ones(n, 1);
y1 = A1\b; 
[x1, conv1] = mygauss_seidel(A1, b, x0, tol, maxit); 
norm(x1 - y1) 
 
iters1 = length(conv1);
i1 = 1:iters1;
 
b = ones(n, 1);
y2 = A2\b; 
[x2, conv2] = mygauss_seidel(A2, b, x0, tol, maxit); 
norm(x2 - y2) 
 
iters2 = length(conv2);
i2 = 1:iters2;
 
b = ones(n, 1);
y3 = A3\b; 
[x3, conv3] = mygauss_seidel(A3, b, x0, tol, maxit); 
norm(x3 - y3) 
 
iters3 = length(conv3);
i3 = 1:iters3;
plot(i1, log(conv1), 'ro-')
hold on
%plot(i1, -1.25*i1, 'r--')
%hold off
 
plot(i2, log(conv2), 'bo-')
%hold on
%plot(i2, -1.25*i2, 'r--')
%hold off
 
plot(i3, log(conv3), 'go-')
%hold on
%plot(i3, -1.25*i3, 'r--')
hold off