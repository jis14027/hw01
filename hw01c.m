A = bucky();
[n, ~] = size(A);
A = A + 5*speye(n); 
 
x0 = ones(n, 1);
tol = 0.0000000001;
maxit = 100;
 
b1 = rand(n, 1);
y1 = A\b1; 
[x1, conv1] = mygauss_seidel(A, b1, x0, tol, maxit); 
norm(x1 - y1) 
 
iters1 = length(conv1);
i1 = 1:iters1;
 
b2 = randn(n, 1);
y2 = A\b2; 
[x2, conv2] = mygauss_seidel(A, b2, x0, tol, maxit); 
norm(x2 - y2) 
 
iters2 = length(conv2);
i2 = 1:iters2;
 
b3 = ones(n, 1);
y3 = A\b3; 
[x3, conv3] = mygauss_seidel(A, b3, x0, tol, maxit); 
norm(x3 - y3) 
 
iters3 = length(conv3);
i3 = 1:iters3;
 
plot(i1, log(conv1), 'ro-')
hold on
%plot(i1, -1.25*i1, 'r--')
%hold off
 
A = bucky();
[n, ~] = size(A);
A = A + 5*speye(n); 
 
x0 = ones(n, 1);
tol = 0.0000000001;
maxit = 100;
 
b1 = rand(n, 1);
y1 = A\b1; 
[x1, conv1] = mygauss_seidel(A, b1, x0, tol, maxit); 
norm(x1 - y1) 
 
iters1 = length(conv1);
i1 = 1:iters1;
 
b2 = randn(n, 1);
y2 = A\b2; 
[x2, conv2] = mygauss_seidel(A, b2, x0, tol, maxit); 
norm(x2 - y2) 
 
iters2 = length(conv2);
i2 = 1:iters2;
 
b3 = ones(n, 1);
y3 = A\b3; 
[x3, conv3] = mygauss_seidel(A, b3, x0, tol, maxit); 
norm(x3 - y3) 
 
iters3 = length(conv3);
i3 = 1:iters3;
 
plot(i1, log(conv1), 'ro-')
hold on
%plot(i1, -1.25*i1, 'r--')
%hold off
 
