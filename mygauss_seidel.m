function [x, conv] = mygauss_seidel(A, b, x0, tol, maxiter)
    [n, ~] = size(A);
    d = diag(A);
    for i = 1:n
        A(i, i) = 0;
    end
    b = b ./ d;
    A = A ./ d;
    conv = [];
    iter = 0;
    rel = 2*tol;
    x = x0;
    while (rel > tol) && (iter < maxiter)
        for j = 1:n
            x(j) = b(j) - A(j,:)*x;
        end
        rel = norm(x - x0)/norm(x0);
        conv = [conv, rel];
        iter = iter + 1;
        x0 = x;
    end
end