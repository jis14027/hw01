A = bucky();
[n, ~] = size(A);
A = A + 5*speye(n);  
b = rand(n, 1);
y = A\b; 
x0 = ones(n, 1);
tol = 0.0000000001;
maxit = 100;
 
[x, conv1] = mygauss_seidel(A, b, x0, tol, maxit); 
norm(x - y) 
 
iters1 = length(conv1);
i1 = 1:iters1;
 
plot(i1, log(conv1), 'ro-')
hold on
%plot(i, -1.25*i, 'r--')
%hold off
 
[x, conv2] = myjacobi(A, b, x0, tol, maxit); 
norm(x - y) 
 
iters2 = length(conv2);
i2 = 1:iters2;
 
plot(i2, log(conv2), 'go-')
%hold on
A = bucky();
[n, ~] = size(A);
A = A + 5*speye(n);  
b = rand(n, 1);
y = A\b; 
x0 = ones(n, 1);
tol = 0.0000000001;
maxit = 100;
 
[x, conv1] = mygauss_seidel(A, b, x0, tol, maxit); 
norm(x - y) 
 
iters1 = length(conv1);
i1 = 1:iters1;
 
plot(i1, log(conv1), 'ro-')
hold on
%plot(i, -1.25*i, 'r--')
%hold off
 
[x, conv2] = myjacobi(A, b, x0, tol, maxit); 
norm(x - y) 
 
iters2 = length(conv2);
i2 = 1:iters2;
 
plot(i2, log(conv2), 'go-')
%hold on
